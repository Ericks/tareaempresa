﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.empresa.entities
{
    class Puesto
    {
        public int id { get; set; }
        public String nombre { get; set; }
        public String abreviacion { get; set; }

        public override String ToString()
        {
            return nombre + "-" + abreviacion;
        }
    }
}
