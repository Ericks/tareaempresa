﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.empresa.entities
{
    class Colaborador
    {
        public int id { get; set; }
        public String cedula { get; set; }
        public String nombre { get; set; }
        public char genero { get; set; }
        public DateTime nacimiento { get; set; }
        public int edad { get; set; }
        public DateTime ingreso { get; set; }
        public char nivelIngles { get; set; }
        public Puesto puesto { get; set; }
        public Departamento departamento { get; set; }


    }
}
