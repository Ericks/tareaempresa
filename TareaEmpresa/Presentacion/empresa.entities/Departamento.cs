﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.empresa.entities
{
    class Departamento
    {
        public int id { get; set; }
        public String nombre { get; set; }

        public override String ToString()
        {
            return id + " - " + nombre;
        }
    }
}
