﻿using Datos.empresa.dao;
using Negocio.empresa.entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.empresa.dao
{
    class DepartamentoBD
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public List<Departamento> ConsultarDatos()
        {
            conexionRetorno = conexion.ConexionBD();
            List<Departamento> lista = new List<Departamento>();
            cmd = new NpgsqlCommand("SELECT id, nombre FROM departamento order by id", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();

            return lista;
        }

        private Departamento cargar(NpgsqlDataReader reader)
        {
            Departamento d = new Departamento()
            {
                id = reader.GetInt32(0),
                nombre = reader.GetString(1)
            };
            return d;
        }
    }
}

