﻿using Presentacion.empresa.gui;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
            
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnMaximizar.Visible = false;
            btnRestaurar.Visible = true;
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            SubmenuReportes.Visible = true;
          
        }

        private void btnDepartamento_Click(object sender, EventArgs e)
        {
          
            SubmenuReportes.Visible = false;
            AbrirFormHijo(new Departamento());
        }

        private void btnIngles_Click(object sender, EventArgs e)
        {
       
            SubmenuReportes.Visible = false;
            AbrirFormHijo(new NivelIngles());
        }

        private void btnFechaIngreso_Click(object sender, EventArgs e)
        {
           
            SubmenuReportes.Visible = false;
            AbrirFormHijo(new FechaIngreso());
        }

        private void btnCantidad_Click(object sender, EventArgs e)
        {
            
            SubmenuReportes.Visible = false;
            AbrirFormHijo(new Cantidad());
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new Mantenimiento());
        }

        private void btnSalir_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void AbrirFormHijo(object formHijo)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);

            Form fh = formHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(fh);
            this.PanelContenedor.Tag = fh;
            fh.Show();

        }
    }
    }


